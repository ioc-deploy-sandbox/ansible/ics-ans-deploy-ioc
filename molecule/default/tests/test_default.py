import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ioc_hosts')


def test_conserver(host):
    conserver = host.service('conserver')
    assert conserver.is_enabled
    assert conserver.is_running
    cmd = host.run('console -u')
    assert cmd.rc == 0
    assert len(cmd.stdout.splitlines()) == 2
    for line in cmd.stdout.splitlines():
        assert line.split()[0] in ['wl-demo-ioc-01', 'wl-demo-ioc-02']
        assert line.split()[1] == 'up'


def test_iocs(host):
    for ioc in ['wl-demo-ioc-01', 'wl-demo-ioc-02']:
        ioc_service = host.service('ioc@' + ioc)
        assert ioc_service.is_enabled
        assert ioc_service.is_running
